# Reactイベントカレンダー

## introduction

カレンダーを作りたいと思ってググって見つけました。
JavaScript で作成されていたので、TypeScript で作り直しました。
あまり使う機会がなかった**useContext**と**useReducer**を極限（自分的には）まで使うことができたと思います。
componentは大きな違いがありませんが、肝心のところである contextはかなり修正しました。

RTK + Dayjs + TypeScriptで作成されていますが、現在はContextでstateを管理しているため、RTKを使う意味があまりありません。
今後Contextで管理しているstateをReduxで管理するように変更しようと考えています。

具体的には以下のことをやります。

* STEP1ではuseContext + useReducerの組み合わせでstateを管理する。
* STEP2ではRTKのRedux + Thunkでstateを管理する。
* STEP3ではRTKのRedux + RTK Queryでstateを管理する。

## posts

- https://almonta2021blog.com/react-calendar-javascript/
