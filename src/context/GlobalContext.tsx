import dayjs, { Dayjs } from "dayjs";
import React, {
  ReactNode,
  createContext,
  useEffect,
  useReducer,
  useState,
} from "react";

import { CalendarAction, CalendarEvent } from "../types";

interface GlobalContextType {
  monthIndex: number;
  setMonthIndex: (index: number) => void;
  daySelected: Dayjs;
  setDaySelected: (day: Dayjs) => void;
  showEventModal: boolean;
  setShowEventModal: (visible: boolean) => void;
  dispatchCalEvent: (event: CalendarAction) => void;
  savedEvents: Array<CalendarEvent>;
  selectedEvent: CalendarEvent | null;
  setSelectedEvent: (event: CalendarEvent) => void;
  prevMonthHandler: () => void;
  nextMonthHandler: () => void;
  resetHandler: () => void;
}

const saveEventsReducer = (
  state: Array<CalendarEvent>,
  action: CalendarAction
) => {
  switch (action.type) {
    case "push":
      return [...state, action.payload];
    case "update":
      return state.map((event) =>
        event.id === action.payload.id ? action.payload : event
      );
    case "delete":
      return state.filter((event) => event.id !== action.payload.id);
    default:
      throw new Error();
  }
};

const initEvents = () => {
  const storageEvents = localStorage.getItem("savedEvents");
  const parsedEvents: Array<CalendarEvent> = storageEvents
    ? JSON.parse(storageEvents)
    : [];
  return parsedEvents;
};

const GlobalContext = createContext<GlobalContextType>({} as GlobalContextType);

export const GlobalContextProvider: React.FC<{ children: ReactNode }> = (
  props
) => {
  const [monthIndex, setMonthIndex] = useState<number>(0);
  const [daySelected, setDaySelected] = useState(dayjs());
  const [showEventModal, setShowEventModal] = useState(false);
  const [selectedEvent, setSelectedEvent] = useState<CalendarEvent | null>(
    null
  );
  const [savedEvents, dispatchCalEvent] = useReducer(
    saveEventsReducer,
    Array<CalendarEvent>,
    initEvents
  );

  useEffect(() => {
    localStorage.setItem("savedEvents", JSON.stringify(savedEvents));
  }, [savedEvents]);

  useEffect(() => {
    if (!showEventModal) {
      setSelectedEvent(null);
    }
  }, [showEventModal]);

  const prevMonthHandler = () => {
    setMonthIndex(monthIndex - 1);
  };
  const nextMonthHandler = () => {
    setMonthIndex(monthIndex + 1);
  };
  const resetHandler = () => {
    setMonthIndex(dayjs().month());
  };

  const context = {
    monthIndex,
    setMonthIndex,
    daySelected,
    setDaySelected,
    showEventModal,
    setShowEventModal,
    savedEvents,
    dispatchCalEvent,
    selectedEvent,
    setSelectedEvent,
    prevMonthHandler,
    nextMonthHandler,
    resetHandler,
  };

  return (
    <GlobalContext.Provider value={context}>
      {props.children}
    </GlobalContext.Provider>
  );
};

export default GlobalContext;
