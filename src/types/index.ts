export interface CalendarEvent {
  title: string;
  day: number;
  id: number;
}

export interface CalendarAction {
  type: string;
  payload: CalendarEvent;
}
