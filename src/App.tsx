import { Dayjs } from "dayjs";
import React, { useContext, useEffect, useState } from "react";

import "./App.css";
import { CalendarHeader } from "./components/CalendarHeader";
import { EventModal } from "./components/EventModal";
import { Month } from "./components/Month";
import GlobalContext from "./context/GlobalContext";
import { getMonth } from "./util";

function App() {
  const month = getMonth();
  console.table(month);

  const [currentMonth, setCurrentMonth] = useState<Dayjs[][]>(month);
  const { monthIndex, showEventModal } = useContext(GlobalContext);

  useEffect(() => {
    setCurrentMonth(getMonth(monthIndex));
  }, [monthIndex]);

  return (
    <>
      {showEventModal && <EventModal />}
      <div className="h-screen flex flex-col">
        <CalendarHeader />
        <div className="flex flex-1">
          <Month month={currentMonth} />
        </div>
      </div>
    </>
  );
}

export default App;
