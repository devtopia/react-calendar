import dayjs, { Dayjs } from "dayjs";
import React, { useContext, useEffect, useState } from "react";

import GlobalContext from "../context/GlobalContext";
import { CalendarEvent } from "../types";

interface Props {
  day: Dayjs;
  rowIdx: number;
}

export const Day: React.FC<Props> = (props) => {
  const { day, rowIdx } = props;
  const [dayEvents, setDayEvents] = useState<CalendarEvent[]>([]);
  const { setDaySelected, setShowEventModal, savedEvents, setSelectedEvent } =
    useContext(GlobalContext);

  const getCurrentDayClass = () => {
    return day.format("DD-MM-YY") === dayjs().format("DD-MM-YY")
      ? "bg-blue-600 text-white rounded-full w-7"
      : "";
  };

  useEffect(() => {
    const events = savedEvents.filter(
      (event) => dayjs(event.day).format("DD-MM-YY") === day.format("DD-MM-YY")
    );
    setDayEvents(events);
  }, [savedEvents, day]);

  return (
    <div className="border border-gray-200 flex flex-col">
      <header className="flex flex-col items-center">
        {rowIdx === 0 && <p className="text-sm mt-1">{day.format("ddd")}</p>}
        <p className={`text-sm p-1 my-1 text-center ${getCurrentDayClass()}`}>
          {day.format("DD")}
        </p>
      </header>
      <div
        className="flex-1 cursor-pointer"
        onClick={() => {
          setDaySelected(day);
          setShowEventModal(true);
        }}
      >
        {dayEvents &&
          dayEvents.map((event, index) => (
            <div
              key={index}
              onClick={() => setSelectedEvent(event)}
              className={
                "bg-neutral-200 p-1 mr-3 text-gray-600 text-sm rounded mb-1 truncate"
              }
            >
              {event.title}
            </div>
          ))}
      </div>
    </div>
  );
};
