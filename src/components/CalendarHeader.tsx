import dayjs from "dayjs";
import ja from "dayjs/locale/ja";
import ko from "dayjs/locale/ko";
import React, { useContext } from "react";
import { MdChevronLeft, MdChevronRight } from "react-icons/md";

import GlobalContext from "../context/GlobalContext";

dayjs.locale(ko);

export const CalendarHeader = () => {
  const { monthIndex, prevMonthHandler, nextMonthHandler, resetHandler } =
    useContext(GlobalContext);

  return (
    <header className="px-4 py-2 flex items-center">
      <h1 className="mr-10 text-xl text-gray-500 font-bold">Calendar</h1>
      <button onClick={resetHandler} className="border rounded py-2 px-4 mr-5">
        Today
      </button>
      <button onClick={prevMonthHandler}>
        <span className="cursor-pointer text-gray-600 mx-2">
          <MdChevronLeft />
        </span>
      </button>
      <button onClick={nextMonthHandler}>
        <span className="cursor-pointer text-gray-600 mx-2">
          <MdChevronRight />
        </span>
      </button>
      <h2 className="ml-4 text-xl text-gray-500 font-bold">
        {dayjs(new Date(dayjs().year(), monthIndex)).format("MMMM YYYY")}
      </h2>
    </header>
  );
};
