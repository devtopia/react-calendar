import { Dayjs } from "dayjs";
import React, { Fragment } from "react";

import { Day } from "./Day";

interface Props {
  month: Dayjs[][];
}

export const Month: React.FC<Props> = (props) => {
  const { month } = props;
  return (
    <div className="flex-1 grid grid-cols-7 grid-rows-5">
      {month &&
        month.map((row, i) => (
          <Fragment key={i}>
            {row.map((day, idx) => (
              <Day day={day} key={idx} rowIdx={i} />
            ))}
          </Fragment>
        ))}
    </div>
  );
};
